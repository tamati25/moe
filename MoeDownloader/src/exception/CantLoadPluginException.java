package exception;

/**
 * Created by tama on 7/9/14.
 */
public class CantLoadPluginException extends Exception {

    private String pluginName;

    public CantLoadPluginException(String pluginName)
    {
        this.pluginName = pluginName;
    }

    @Override
    public String getMessage()
    {
        return "Can't load plugin " + pluginName;
    }
}
