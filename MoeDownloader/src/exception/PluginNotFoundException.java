package exception;

/**
 * Created by tama on 7/9/14.
 */
public class PluginNotFoundException extends Exception {

    private String url;

    public PluginNotFoundException(String url)
    {
        this.url = url;
    }
    @Override
    public String getMessage()
    {
        return "No plugin found to handle the link given : " + url;
    }
}
