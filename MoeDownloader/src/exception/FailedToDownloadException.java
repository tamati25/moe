package exception;

/**
 * Created by tama on 7/10/14.
 */
public class FailedToDownloadException extends Exception {
    Exception innerException;
    public FailedToDownloadException(Exception e)
    {
        this.innerException = e;
    }
    public String getMessage()
    {
        return "Failed to download : " + innerException.getClass().getSimpleName();
    }
}
