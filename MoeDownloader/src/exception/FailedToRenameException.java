package exception;

/**
 * Created by tama on 7/9/14.
 */
public class FailedToRenameException extends Exception {
    @Override
    public String getMessage()
    {
        return "Failed to rename downloaded file";
    }
}
