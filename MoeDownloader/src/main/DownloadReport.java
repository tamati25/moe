package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tama on 7/9/14.
 */
public class DownloadReport
{
    private List<String> success;
    private List<String> failed;

    public DownloadReport()
    {
        success = new ArrayList<String>();
        failed = new ArrayList<String>();
    }
    public void success(String title)
    {
        this.success.add(title);
    }

    public void failed(String title)
    {
        this.failed.add(title);
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("------Report------\n");

        sb.append("Success :\n");
        for (String s : success)
        {
            sb.append("\t" + s + "\n");
        }
        sb.append("\tTotal : " + success.size());
        sb.append("\n\n");

        sb.append("Failed :\n");
        for (String s : failed)
        {
            sb.append("\t" + s + "\n");
        }
        sb.append("\tTotal : " + failed.size());
        sb.append("\n\n");

        int total = success.size() + failed.size();
        sb.append("-- Total attempts : ");
        sb.append(String.valueOf(total));

        return sb.toString();
    }
}
