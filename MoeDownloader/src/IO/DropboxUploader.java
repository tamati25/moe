package IO;

import com.dropbox.core.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by tama on 6/27/14.
 */
public class DropboxUploader {

    public static boolean upload(File f, String accessToken, String folder)
    {
        FileInputStream is = null;
        try
        {
            is = new FileInputStream(f);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        if (is == null)
        {
            return false;
        }

        //Connect to dropbox
        DbxRequestConfig config = new DbxRequestConfig("Moe downloader v2", Locale.getDefault().toString());
        DbxClient client = new DbxClient(config, accessToken);

        try
        {
            DbxEntry.File uploadedFile = client.uploadFile(folder + f.getName(), DbxWriteMode.add(), f.length(), is);
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
        catch (DbxException dbe)
        {
            dbe.printStackTrace();
            return false;
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
