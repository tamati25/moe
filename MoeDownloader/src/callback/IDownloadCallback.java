package callback;

/**
 * Created by tama on 7/9/14.
 */
public interface IDownloadCallback
{
    public void onSuccess(String title);
    public void onError(String title, Exception e);
}
