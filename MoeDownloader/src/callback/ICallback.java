package callback;

import java.io.File;

/**
 * Created by tama on 7/9/14.
 */
public interface ICallback
{
    public void onDownloadFinished(File downloaded);
}
