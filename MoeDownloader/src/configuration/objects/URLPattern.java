package configuration.objects;

import java.util.regex.Pattern;

/**
 * Created by tama on 6/20/14.
 */
public class URLPattern {
    public Pattern pattern;
    public String pluginName;
    public boolean active;
}
