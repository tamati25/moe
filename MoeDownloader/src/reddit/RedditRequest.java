package reddit;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import util.IOUtils;

import java.io.IOException;

/**
 * Created by tama on 6/20/14.
 */
public class RedditRequest {

    private static int RESPONSE_OK = 200;

    private static String API_URL = "https://oauth.reddit.com/";
    private String token;

    public RedditRequest(String token)
    {
        this.token = token;
    }

    public String requestUrl(String url) throws IOException {
        String content = null;

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(API_URL + url);
        httpGet.addHeader("Authorization", "bearer " + token);
        httpGet.addHeader("User-agent", "moe downloader bot v2");
        HttpResponse response = client.execute(httpGet);

        //Check the response status
        if (response.getStatusLine() != null && response.getStatusLine().getStatusCode() != RESPONSE_OK)
        {
            StatusLine status = response.getStatusLine();
            System.err.println("Response " + status.getStatusCode() + " (" + status.getReasonPhrase() + ")");
            return null;
        }

        if (response != null && response.getEntity() != null)
        {
            content = IOUtils.getStringFromInputStream(response.getEntity().getContent());
        }

        return content;
    }

    public static void main(String[] args)
    {
        String aToken = "igJpcqTrPXodvE7HLSv6nkWHpEE";
        RedditRequest request = new RedditRequest(aToken);
        try {
            String content = request.requestUrl("/user/tama_92/liked");
            System.out.println(content);
        } catch (IOException e) {
            System.err.println("Failed :(");
        }
    }
}
