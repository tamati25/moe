package reddit.json;

/**
 * Created by tama on 6/25/14.
 */
public class ResponseChildData {
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public ChildData getData() {
        return data;
    }

    public void setData(ChildData data) {
        this.data = data;
    }

    private String kind;
    private ChildData data;
}
