package reddit.json;

/**
 * Created by tama on 6/25/14.
 */
public class Response {
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    private String kind;
    private ResponseData data;
}
