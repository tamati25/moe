package reddit.json;

/**
 * Created by tama on 6/25/14.
 */
public class RedditJSON {
    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    private Response response;
}
