package reddit.json;

import java.util.List;

/**
 * Created by tama on 6/25/14.
 */
public class ResponseData {
    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    private String modhash;

    public List<ResponseChildData> getChildren() {
        return children;
    }

    public void setChildren(List<ResponseChildData> children) {
        this.children = children;
    }

    private List<ResponseChildData> children;
}
