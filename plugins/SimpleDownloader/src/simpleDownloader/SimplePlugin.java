package simpleDownloader;

import interfaces.IPlugin;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tama on 6/20/14.
 *
 * The most simple webpage type : a direct link to an image
 */
public class SimplePlugin implements IPlugin {

    @Override
    public List<String> getLinks(String link) {
        List<String> result = new ArrayList<String>();
        result.add(link);
        return result;
    }
}
