package imgurAlbum;

import interfaces.IPlugin;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tama on 6/30/14.
 *
 * Get links from ImgurAlbum type webpage (http(s)://imgur.com/a/...)
 */
public class ImgurAlbum implements IPlugin {

    public List<String> getLinks(String url)
    {
        List<String> result = new ArrayList<String>();
        try
        {
            Document doc = Jsoup.connect(url).get();
            // Credit : https://github.com/maluramichael/reddit-imgur-scraper/blob/master/src/main/java/de/devnetik/reddit/crawler/runnables/parser/ParseImgurAlbumRunnable.java
            Elements images = doc.select("img[data-src]:not([id*=thumb-])");

            if (images != null)
            {
                for (Element image : images)
                {
                    result.add("http:" + image.attr("data-src"));
                }
            }
        }
        catch (IOException e)
        {
            //System.out.println("Failed :( " + e.getMessage());
        }

        return result;
    }
}
