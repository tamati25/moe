package redditBooruGallery;

import interfaces.IPlugin;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tama on 6/29/14.
 *
 * Get links from RedditBooruGallery type webpages (http://awwnime.redditbooru.com/gallery/...)
 */
public class RedditBooruGallery implements IPlugin {

    private String IMAGE_URL_TAG = "data-full";

    List<String> images = new ArrayList<String>();
    private int index = 0;

    @Override
    public List<String> getLinks(String url) {

        System.out.println("[RedditBooruGallery] Setting up");

        List<String> result = new ArrayList<String>();

        /* Get the links using Jsoup */
        try
        {
            Document doc = Jsoup.connect(url).get();

            //The images are located in the 'script' part of the html
            Element script = doc.select("script").first();

            Pattern p = Pattern.compile("\"cdnUrl\":\"(.*?)\",");
            Matcher m = p.matcher(script.html());

            while (m.find())
            {
                String imgUrl = m.group(1);
                //System.out.print(imgUrl + " -> ");
                imgUrl = imgUrl.replaceAll("\\\\", "");

                //System.out.println(imgUrl);
                result.add(imgUrl);
            }
        }
        catch (IOException e)
        {
            System.out.println("[RedditBooruGallery] Failed : " + e.getMessage());
        }

        return result;
    }
}
