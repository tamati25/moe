package interfaces;

import java.io.InputStream;
import java.util.List;

/**
 * Created by tama on 6/20/14.
 *
 * Plugin interface : Parse a web page html to find the relevant image(s) link(s)
 */
public interface IPlugin {
    /** Get the links present in a webpage given by its url */
    public List<String> getLinks(String url);
}
